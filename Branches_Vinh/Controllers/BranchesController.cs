﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Branches_Vinh.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
       



    
        private readonly DataContext _context;

        public BranchesController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]

        public async Task<ActionResult<List<Branches>>> Get()
        {
            

            return Ok(await _context.branches.ToListAsync());
        }
        [HttpGet("{id}")]

        public async Task<ActionResult<Branches>> Get(int id)
        {
            var branch = await _context.branches.FindAsync(id);

            if (branch == null)
            {
                return BadRequest("Not found");
            }

            return Ok(branch);
        }

        [HttpPost]
        public async Task<ActionResult<List<Branches>>> AddBranch(Branches branch)
        {

            _context.branches.Add(branch);
            await _context.SaveChangesAsync();

            return Ok(await _context.branches.ToListAsync());
        }
        [HttpPut]

        public async Task<ActionResult<List<Branches>>> UpdateBranch(Branches request)
        {
            var branch =await _context.branches.FindAsync(request.BranchId);

            if (branch == null)
                return BadRequest("Not found");

            branch.Name = request.Name;
            branch.Address = request.Address;
            branch.City = request.City;
            branch.State = request.State;
            branch.ZipCode = request.ZipCode;

            await _context.SaveChangesAsync();
            return Ok(await _context.branches.ToListAsync());
        }
        [HttpDelete("{id}")]

        public async Task<ActionResult<Branches>> Delete(int id)
        {
            var branch = await _context.branches.FindAsync(id);

            if (branch == null)
                return BadRequest("Not found");

            _context.branches.Remove(branch);

            await _context.SaveChangesAsync();
            return Ok(await _context.branches.ToListAsync());
        }
    }
}
