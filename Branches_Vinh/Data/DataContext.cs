﻿using Microsoft.EntityFrameworkCore;

namespace Branches_Vinh.Data
{
    public class DataContext : DbContext

    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }


        public DbSet<Branches> branches { get; set; }
    }
}
